# blog_template

This blog method uses the Hugo static-site generator. The content of this repo is modified from the example content at: https://gitlab.com/pages/hugo




Site-creation Instructions


**INITIAL SETUP**
The following instructions install all plugins and extras required to build static pages in GitLab. They will need to be executed on all machines contributing to the blog. This should only need to done once per computer. 

================

*forMac:*
Install Homebrew (macOS package manager) 
-Open Terminal
-Run: /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

*forWindows:*
Install Chocolatey (windows package manager)
-Open Command Prompt as Administrator
-Run: @"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

*forLinux:*
Use Native Package Manager for Your Linux Build (varies based on preferred OS)
-e.g. for Debian or Ubuntu use apt-get, for Arch pacman, for Solus use eopkg, etc.

================

Install Git Command-Line Tools

*forMac:*
-If you use Xcode, git is included in the Xcode packages
-If not, in Terminal, run: brew install git

*forWindows:*
-In Command Prompt (as Admin), run: choco install git.install

*forLinux:*
-In Terminal, run sudo <yourPackageManager> update
-e.g. sudo apt-get update, sudo eopkg update, etc.
-Followed by: sudo <yourPackageManager> upgrade
-Followed by: sudo <yourPackageManager> install git

================

Install Hugo (static site generator) 

*forMac:*
-In Terminal, run: brew install hugo

*forWindows:*
-In Command Prompt (as Admin), run: choco install hugo -confirm

*forLinux:*
-In Terminal, run: sudo <yourPackageManager> update
-Followed by: sudo <yourPackageManager> install hugo

================

Install Runners (git push updates webpage) 

*forMac:*
-In Terminal, run: brew install gitlab-runner

*forWindows:*
-In Command Prompt (as Admin), run: choco install gitlab-runner
-Press Y (yes) when prompted

*forLinux:*
-In Terminal, run: sudo <yourPackageManager> update
-Followed by: sudo <yourPackageManager> install gitlab-runner

================

Updating Packages (This will update the package managers and any software installed with them)

*forMac:*
-In Terminal run: brew update
-Followed by: brew upgrade

*forWindows:*
-In Command Prompt (as Admin), run: choco upgrade chocolatey

*forLinux:*
-In Terminal, run: sudo <yourPackageManager> update
-Followed by: sudo <yourPackageManager> upgrade

================

**CREATE NEW BLOG SITE**
These instructions are for the initial setup for a new blog site. This should only need to be done once per site. This process should be the same for Mac, Windows, and Linux.

================ 

FOR REPOSITORY OWNER ONLY
Create New Repo
-Go to gitlab.com
-Login and create New Group (this is if you have multiple projects to be released under the same heading, skip to New Project if you are only blogging)
-Name the Group and path.
-Add a description and avatar if your group has one
-Set Visibility Level to Public

-Create New Project in the group. 
-Give the project a name (blog, for example)
-Give a project description and set Visibility to Public
-Initialize with a README
-Click Create project

================

Clone Project to Local Machine
-Next to the project url, change SSH to HTTPS and copy the url. 
-In Terminal or Command Prompt, cd to the location you wish to store the project. 
-type:  git clone <repoURL>
-press Return and let the repo copy 
-cd into the repo directory

================

Add/Edit the Provided Files
-Copy all files and folders from blog-template into the new repo folder. 
-Open the config.toml file
-Change baseurl to “https://yourgroupname.gitlab.io/blog”
-Change Author info (more social media options are available, see themes/beautifulhugo/data/beautifulhugo/social.toml to check the list)
-Customize menu options to your liking
-In Terminal or Command Prompt, git add all of the copied content
-git commit -m “your commit message”
-git push

================

Confirm Site Updates
-Go to the repo at gitlab.com
-From the left sidebar, select CI/CD -> Pipelines
-Wait for the Running process to Pass (this can take a bit depending on the amount of content being pushed)
-From the left sidebar, select Settings -> Pages
-Under Access Pages, you should see a ‘Congratulations’ message, if this is true click the provided hyperlink. This should take you to the updated blog page to view titles and formatting.

================

FOR USERS & CONTRIBUTERS
Clone Project to Local Machine (This will need to be done on all contributing machines)
-Next to the project url, change SSH to HTTPS and copy the url. 
-In Terminal or Command Prompt, cd to the location you wish to store the project. 
-type:  git clone <repoURL>
-press Return and let the repo copy 
-cd into the repo directory

================

**ADD NEW CONTENT**
This is the repeated process. These instructions will be done for every new blog post. I will see if I can figure out a bash script to update the config file, but this process works for now. This process should be the same for Mac, Windows, and Linux.

================

Make Sure You Have the Most Recent Repository
-In Terminal or Command Prompt, cd to the repo directory
-Type: git pull and press Return

Create New Post
-In the local repository folder, go to content/post
-Create new .md file (or duplicate the template)  [.md = markdown]
-Name the file with the proper format (YYYY-MM-DD-name-of-post.md)
-e.g. 2018-09-22-my-first-blog-entry.md would be written on September 22, 2018
-Follow the included template for adding content (title, subtitle, posting date, relevant tags, text pics, etc)
-Save the edited .md file

================

Update config.toml
-Open the config.toml file. 
-Scroll down to the menu area and add a new entry below your latest entry. Format as:

[[menu.main]]
	parent = “posts”
	name = “Name of Post”
	url = “post/YYYY-MM-DD-name-of-post”
	weight = n+1 (n = weight of previous entry)

-Save config.toml

================

Push New Post
-In Terminal or Command Prompt, cd to local repo directory
-Type: git add config.toml content/  and press Return
-Type: git commit -m “Your commit message”  and press Return
-Type: git push  and press Return
-Check on the project update status on gitlab.com (CI/CD -> Pipelines)
-Once the pipeline reads Passed, the post is published and live. 

================
